
class Employee {
    constructor(name, age, salary) {
        this._name = name,
        this._age = age,
        this._salary = salary
    }

    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }

    get age(){
        return this._age;
    }
    set age(age){
        this._age = age;
    }

    get salary(){
        return this._salary;
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
}

class Programmer extends Employee { 
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }
    
    get salary(){
        return this._salary * 3;
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
}

const programmer1 = new Programmer('John', 25, 3000, 'English');
programmer1.salary = 3000;
console.log(programmer1.salary);
console.log(programmer1);

const programmer2 = new Programmer('Andr', 20, 700, 'French');
programmer2.salary = 700;
console.log(programmer2.salary);
console.log(programmer2);